Warwick - Data Engineering Challenge
====================================

The challenge below is designed to expose a core understanding of data
engineering principles.

If you're not having fun doing this challenge, maybe you are not a good fit for
the position because at a rudimentary level the data engineers at Warwick are
tackling very similar challenges every day.

Now on to the SQL Challenge.
----------------------------

### Objective:

Create a FracFocus dataset and accompanying transformations in the data platform and ETL of
your choosing (FracFocus is a public data set available at
[https://fracfocus.org/](https://fracfocus.org/))

### Guidelines:

-   Create a dataset with the source FracFocus data in its original format
    (staging)

-   Transform the Fracfocus staging data into a second Well Treatment reporting dataset
    with the following facts:

    -   Number of jobs started by supplier by well by month

    -   Max and Min Percentage of Ingredients used per Well

    -   Total Base Water Volume by State, County and Well

-   Add to the reporting dataset data, just for Oklahoma from this external data
    source
    OCC[(ftp://ftp.occeweb.com/OG_DATA/W27base.zip)](ftp://ftp.occeweb.com/OG_DATA/W27base.zip)

    -   The only attributes required from OCC are Well Name, API Number, Well
        Status, Spud Date and First Production Date

    -   Create a query/view to integrate the FracFocus header table with the
        new columns from the Oklahoma OCC wells

### Timeline and discussion:

-   This task should be completed in 5 days

-   After the task is complete we will schedule a discussion so that you can
    walk us through your approach


### Notes

Communication is encouraged. If you have questions about the technical specs,
data or the oil and gas industry don't hesitate to contact us while you are
doing this exercise

[stuart.morrison@warwick-energy.com](stuart.morrison@warwick-energy.com)

This solution can be built on a personal machine or in a cloud service.  If you
don't have any of these readily available please reach out with your technology
preference and we can discuss a solution.