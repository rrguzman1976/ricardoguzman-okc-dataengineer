USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.FactWellIngredient', N'U') IS NOT NULL
	DROP TABLE dbo.FactWellIngredient;
GO

-- Need FactWellIngredient: DimWellKey, DimIngredientKey, PercentHFJob, PercentHFJobMax (derived), PercentHFJobMin (derived)
-- Grain is date, well, ingredient (1 to many)
CREATE TABLE dbo.FactWellIngredient
(
	FactWellIngredientKey		INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, DimDateKey				INT					NOT NULL
	, DimWellKey				INT					NOT NULL
	, DimIngredientKey			INT					NOT NULL
	, [PercentHFJob]			FLOAT				NULL
	, [PercentHFJobMin]			BIT					NULL
	, [PercentHFJobMax]			BIT					NULL
);
GO

INSERT INTO dbo.FactWellIngredient
SELECT	d.DimDateKey
		, w.DimWellKey
		, di.DimIngredientKey
		, i.[PercentHFJob]
		, CASE
			WHEN i.[PercentHFJob] = MIN(i.[PercentHFJob]) OVER (PARTITION BY d.DimDateKey, w.DimWellKey) THEN 1
			ELSE 0
		END AS [PercentHFJobMin]
		, CASE
			WHEN i.[PercentHFJob] = MAX(i.[PercentHFJob]) OVER (PARTITION BY d.DimDateKey, w.DimWellKey) THEN 1
			ELSE 0
		END AS [PercentHFJobMax]
FROM	[FracFocusRegistry].[dbo].[RegistryUpload] AS r
	INNER JOIN [FracFocusRegistry].[dbo].[RegistryUploadPurpose] AS p
		ON r.pKey = p.[pKeyRegistryUpload]
	INNER JOIN [FracFocusRegistry].[dbo].[RegistryUploadIngredients] AS i
		ON p.[pKey] = i.[pKeyPurpose]
	INNER JOIN dbo.DimDate AS d
		ON COALESCE(CAST(FORMAT(r.[JobStartDate], 'yyyyMMdd') AS INT), -1) = d.DimDateKey
	INNER JOIN dbo.DimWell AS w
		ON w.[APINumber] = r.[APINumber]
	INNER JOIN dbo.DimIngredient AS di
		ON di.[IngredientName] = TRIM(i.[IngredientName])
			AND di.[CASNumber] = TRIM(i.[CASNumber])
;

SELECT  f.[DimDateKey]
		, w.[WellName]
		, i.[IngredientName]
		, f.[PercentHFJob]
		, f.[PercentHFJobMin]
		, f.[PercentHFJobMax]
FROM	[dbo].[FactWellIngredient] AS f
	INNER JOIN dbo.DimWell AS w
		ON w.DimWellKey = f.DimWellKey
	INNER JOIN dbo.DimIngredient AS i
		ON f.[DimIngredientKey] = i.[DimIngredientKey]
WHERE	(f.[PercentHFJobMin] = 1 OR f.[PercentHFJobMax] = 1)
GO

/*
SELECT	'[RegistryUpload]' AS [0]
		, r.[pKey]
		, r.[JobStartDate]
		, r.[JobEndDate]
		, r.[APINumber]
		, r.[StateNumber]
		, r.[CountyNumber]
		, r.[OperatorName]
		, r.[WellName]
		, r.[Latitude]
		, r.[Longitude]
		, r.[Projection]
		, r.[TVD]
		, r.[TotalBaseWaterVolume]
		, r.[TotalBaseNonWaterVolume]
		, r.[StateName]
		, r.[CountyName]
		, r.[FFVersion]
		, r.[FederalWell]
		, r.[IndianWell]
		, r.[Source]
		, r.[DTMOD]
		, '[RegistryUploadPurpose]' AS [1]
		, p.[pKey]
		, p.[pKeyRegistryUpload] AS [pKeyRegistryUpload(FK)]
		, p.[TradeName]
		, p.[Supplier]
		, p.[Purpose]
		, p.[SystemApproach]
		, p.[IsWater]
		, p.[PercentHFJob]
		, p.[IngredientMSDS]
		, '[RegistryUploadIngredients]' AS [2]
		, i.[pKey]
		, i.[pKeyPurpose] AS [pKeyPurpose(FK)]
		, i.[IngredientName]
		, i.[CASNumber]
		, i.[PercentHighAdditive]
		, i.[PercentHFJob]
		, i.[IngredientComment]
		, i.[IngredientMSDS]
		, i.[MassIngredient]
		, i.[ClaimantCompany]
		, i.[pKeyDisclosure]
FROM	[FracFocusRegistry].[dbo].[RegistryUpload] AS r
	LEFT JOIN [FracFocusRegistry].[dbo].[RegistryUploadPurpose] AS p
		ON r.pKey = p.[pKeyRegistryUpload]
	LEFT JOIN [FracFocusRegistry].[dbo].[RegistryUploadIngredients] AS i
		ON p.[pKey] = i.[pKeyPurpose]
WHERE	TRIM(r.[WellName]) = 'Mumme-TCU Unit #70H'
*/