USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.DimWell', N'U') IS NOT NULL
	DROP TABLE dbo.DimWell;
GO

-- Need DimWell: DimWellKey, APINumber, WellName, OperatorName, TVD, FederalWell, IndianWell
CREATE TABLE dbo.DimWell
(
	DimWellKey			INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, [APINumber]		VARCHAR(14)			NOT NULL
		UNIQUE
	, [WellName]		VARCHAR(150)		NOT NULL
	, [FederalWell]		BIT					NULL
	, [IndianWell]		BIT					NOT NULL
);
GO

WITH dedup
AS
(
	SELECT	[APINumber]
			, [WellName]
			, [FederalWell]
			, [IndianWell]
			, RANK() OVER (PARTITION BY [APINumber]
							ORDER BY JobStartDate DESC, pKey) AS [Ranker]
	FROM	[FracFocusRegistry].[dbo].[RegistryUpload]
)
INSERT INTO dbo.DimWell
SELECT	[APINumber]
		,TRIM([WellName])
		,[FederalWell]
		,[IndianWell]
FROM	dedup
WHERE	[Ranker] = 1;

/*
SELECT	*
FROM	[FracFocusRegistry].[dbo].[RegistryUpload]
WHERE	[APINumber] = '03023104390000'
*/

SELECT	*
FROM	dbo.DimWell;