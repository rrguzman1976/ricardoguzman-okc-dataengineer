USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.FactWellSupplier', N'U') IS NOT NULL
	DROP TABLE dbo.FactWellSupplier;
GO

-- Need FactWell: DimSupplierKey, DimWellKey, JobStartDateKey
-- Grain is Well, Supplier, JobStartDate (1 to many to many)
-- Fact-less fact table
CREATE TABLE dbo.FactWellSupplier
(
	FactWellSupplierKey			INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, DimSupplierKey			INT					NOT NULL
	, DimWellKey				INT					NOT NULL
	, JobStartDateKey			INT					NOT NULL
);
GO

INSERT INTO dbo.FactWellSupplier
SELECT	DISTINCT
		s.DimSupplierKey
		, w.DimWellKey
		, d.DimDateKey
FROM	[FracFocusRegistry].[dbo].[RegistryUpload] AS r
	INNER JOIN [FracFocusRegistry].[dbo].[RegistryUploadPurpose] AS p
		ON r.pKey = p.[pKeyRegistryUpload]
	INNER JOIN dbo.DimDate AS d
		ON COALESCE(CAST(FORMAT(r.[JobStartDate], 'yyyyMMdd') AS INT), -1) = d.DimDateKey
	INNER JOIN dbo.DimWell AS w
		ON r.[APINumber] = w.[APINumber]
	INNER JOIN dbo.DimSupplier AS s
		ON COALESCE(NULLIF(TRIM(p.[Supplier]), ''), 'Unknown') = s.[Supplier]
;

SELECT	s.Supplier
		, w.WellName
		, d.CalendarYear
		, d.MonthOfYear
		, COUNT(*) AS [Number Of Wells Started]
FROM	dbo.FactWellSupplier AS f
	INNER JOIN dbo.DimDate AS d
		ON d.DimDateKey = f.JobStartDateKey
	INNER JOIN dbo.DimWell AS w
		ON w.DimWellKey = f.DimWellKey
	INNER JOIN dbo.DimSupplier AS s
		ON s.DimSupplierKey = f.DimSupplierKey
GROUP BY ROLLUP
(	
	s.Supplier
	, w.WellName
	, d.CalendarYear
	, d.MonthOfYear
)
ORDER BY s.Supplier
		, w.WellName
		, d.CalendarYear
		, d.MonthOfYear
;

/*
SELECT	'[RegistryUpload]' AS [0]
		, r.[pKey]
		, r.[JobStartDate]
		, r.[JobEndDate]
		, r.[APINumber]
		, r.[StateNumber]
		, r.[CountyNumber]
		, r.[OperatorName]
		, r.[WellName]
		, r.[Latitude]
		, r.[Longitude]
		, r.[Projection]
		, r.[TVD]
		, r.[TotalBaseWaterVolume]
		, r.[TotalBaseNonWaterVolume]
		, r.[StateName]
		, r.[CountyName]
		, r.[FFVersion]
		, r.[FederalWell]
		, r.[IndianWell]
		, r.[Source]
		, r.[DTMOD]
		, '[RegistryUploadPurpose]' AS [1]
		, p.[pKey]
		, p.[pKeyRegistryUpload] AS [pKeyRegistryUpload(FK)]
		, p.[TradeName]
		, p.[Supplier]
		, p.[Purpose]
		, p.[SystemApproach]
		, p.[IsWater]
		, p.[PercentHFJob]
		, p.[IngredientMSDS]
FROM	[FracFocusRegistry].[dbo].[RegistryUpload] AS r
	LEFT JOIN [FracFocusRegistry].[dbo].[RegistryUploadPurpose] AS p
		ON r.pKey = p.[pKeyRegistryUpload]
WHERE	TRIM(p.[Supplier]) = 'Ace Completions'
		AND TRIM(r.[WellName]) = 'Maggie 8';
*/