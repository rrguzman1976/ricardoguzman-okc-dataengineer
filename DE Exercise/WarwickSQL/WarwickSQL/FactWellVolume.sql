USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.FactWellVolume', N'U') IS NOT NULL
	DROP TABLE dbo.FactWellVolume;
GO

-- Need Volume: DimWellKey, DimGeographyKey, TotalBaseWaterVolume, TotalBaseNonWaterVolume
-- Grain is well
CREATE TABLE dbo.FactWellVolume
(
	FactWellVolumeKey			INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, DimWellKey				INT					NOT NULL
	, DimGeographyKey			INT					NOT NULL
	, [TotalBaseWaterVolume]	FLOAT				NULL
	, [TotalBaseNonWaterVolume]	FLOAT				NULL
);
GO

INSERT INTO dbo.FactWellVolume
SELECT 	dw.DimWellKey
		, g.DimGeographyKey
		, ISNULL(w.[TotalBaseWaterVolume], 0) AS [TotalBaseWaterVolume]
		, ISNULL(w.[TotalBaseNonWaterVolume], 0) AS [TotalBaseNonWaterVolume]
FROM	[FracFocusRegistry].[dbo].[RegistryUpload] AS w
	INNER JOIN dbo.DimGeography AS g
		ON w.[StateNumber] = g.[StateNumber]
			AND w.[CountyNumber] = g.[CountyNumber]
	INNER JOIN dbo.DimWell AS dw
		ON w.[APINumber] = dw.[APINumber]
GO

SELECT	'FactWellVolume'
		, g.[StateName]
		, g.[CountyName]
		, w.[WellName]
		, SUM(f.[TotalBaseWaterVolume]) AS [TotalBaseWaterVolume]
FROM	dbo.FactWellVolume AS f
	INNER JOIN dbo.DimGeography AS g
		ON f.DimGeographyKey = g.DimGeographyKey
	INNER JOIN dbo.DimWell AS w
		ON f.DimWellKey = w.DimWellKey
GROUP BY ROLLUP
(
	g.[StateName], g.[CountyName], w.[WellName]
);

SELECT 	SUM(ISNULL(w.[TotalBaseWaterVolume], 0)) AS [TotalBaseWaterVolume]
FROM	[FracFocusRegistry].[dbo].[RegistryUpload] AS w
