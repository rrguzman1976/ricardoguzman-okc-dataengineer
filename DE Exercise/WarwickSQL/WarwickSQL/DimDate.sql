USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.DimDate', N'U') IS NOT NULL
	DROP TABLE dbo.DimDate;
GO

CREATE TABLE dbo.DimDate
(
	DimDateKey			INT				NOT NULL
		PRIMARY KEY
	, DateValue			DATE			NULL
	, [DayOfWeek]		INT				NOT NULL
	, [DayOfMonth]		INT				NOT NULL
	, [DayOfYear]		INT				NOT NULL
	, [WeekOfYear]		INT				NOT NULL
	, [CalendarMonth]	NVARCHAR(32)	NOT NULL
	, [MonthOfYear]		INT				NOT NULL
	, [CalendarYear]	INT				NOT NULL
);

DECLARE @START DATETIME2 = '19000101';
DECLARE @END DATETIME2 = '39991231';

-- Use T2 to generate a date sequence
INSERT INTO dbo.DimDate
SELECT	-1, NULL, -1, -1, -1, -1, 'Unknown', -1, -1;

INSERT INTO dbo.DimDate
SELECT	CAST(FORMAT(DATEADD(day, n-1, @START), 'yyyyMMdd') AS INT) AS [DateDimID]
		, CAST(DATEADD(day, n-1, @START) AS DATE) AS [Date]
		, DATEPART(weekday, DATEADD(day, n-1, @START)) AS [DayOfWeek]
		, DATEPART(day, DATEADD(day, n-1, @START)) AS [DayOfMonth]
		, DATEPART(dayofyear, DATEADD(day, n-1, @START)) AS [DayOfYear]
		, DATEPART(week, DATEADD(day, n-1, @START)) AS [WeekOfYear]
		, DATENAME(month, DATEADD(day, n-1, @START)) AS [CalendarMonth]
		, DATEPART(month, DATEADD(day, n-1, @START)) AS [MonthOfYear]
		, DATEPART(year, DATEADD(day, n-1, @START)) AS [CalendarYear]
FROM	dbo.Nums
WHERE	[n] <= DATEDIFF(day, @START, @END) + 1
ORDER BY [n];

SELECT	TOP (10)
		*
FROM	dbo.DimDate
ORDER BY DimDateKey;