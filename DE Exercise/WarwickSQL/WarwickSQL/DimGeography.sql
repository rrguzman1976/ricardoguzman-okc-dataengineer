USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.DimGeography', N'U') IS NOT NULL
	DROP TABLE dbo.DimGeography;
GO

-- Need DimGeography: DimGeographyKey, Country (default United States), State, County
CREATE TABLE dbo.DimGeography
(
	DimGeographyKey		INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, CountryName		VARCHAR(50)			NOT NULL
	, [StateName]		VARCHAR(50)			NULL
	, [CountyName]		VARCHAR(50)			NULL
	, [StateNumber]		VARCHAR(2)			NULL
	, [CountyNumber]	VARCHAR(3)			NULL
	, CONSTRAINT IX_DIMGEOGRAPHY_COUNTY
		UNIQUE ([StateNumber], [CountyNumber]) 
);
GO

WITH dedup
AS
(
	SELECT	[StateName]
			, [CountyName]
			, [StateNumber]
			, [CountyNumber]
			, RANK() OVER (PARTITION BY [StateNumber], [CountyNumber]
							ORDER BY JobStartDate DESC, pKey) AS Ranker
	FROM	[FracFocusRegistry].[dbo].[RegistryUpload]
)
INSERT INTO dbo.DimGeography
SELECT	'United States' AS CountryName
		, TRIM([StateName])
		, TRIM([CountyName])
		, [StateNumber]
		, [CountyNumber]
FROM	dedup
WHERE	Ranker = 1;
GO

/*
SELECT	DISTINCT
		'United States' AS CountryName
		,[StateName]
		,[CountyName]
		,[StateNumber]
		,[CountyNumber]
FROM	[FracFocusRegistry].[dbo].[RegistryUpload]
WHERE	[StateNumber] = '03' AND [CountyNumber] = '023'
*/

SELECT	*
FROM	dbo.DimGeography;