USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.DimPurpose', N'U') IS NOT NULL
	DROP TABLE dbo.DimPurpose;
GO

-- Need DimPurpose: DimPurposeKey, TradeName, Purpose
CREATE TABLE dbo.DimPurpose
(
	DimPurposeKey		INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, [TradeName]		VARCHAR(250)		NULL
	, [Purpose]			VARCHAR(250)		NOT NULL
	, CONSTRAINT IX_DIMPURPOSE_TRADENAME
		UNIQUE ([TradeName], [Purpose])
);
GO

WITH cleanse
AS
(
	SELECT	CASE
				WHEN [TradeName] IS NULL THEN [Purpose]
				WHEN TRIM([TradeName]) = '' THEN [Purpose]
				ELSE TRIM([TradeName])
			END [TradeName]
			,[Purpose]
	FROM	[FracFocusRegistry].[dbo].[RegistryUploadPurpose]
)
INSERT INTO dbo.DimPurpose
SELECT	DISTINCT [TradeName]
		,[Purpose]
FROM	cleanse;
GO

/*
SELECT	*
FROM	[FracFocusRegistry].[dbo].[RegistryUploadPurpose]
WHERE	[TradeName] = '% 15 HYDROCHLORIC ACID'
*/

SELECT	*
FROM	dbo.DimPurpose;

