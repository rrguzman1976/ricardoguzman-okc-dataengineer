USE [WarwickDB]
GO

-- TODO: Need DimTime
-- TODO: Need DimSpatial: DimSpatialKey, Latitude, Longitude, Projection

USE [FracFocusRegistry]
GO

SELECT	TOP (1000)
		'[RegistryUpload]' AS [0]
		, r.[pKey]
		, r.[JobStartDate]
		, r.[JobEndDate]
		, r.[APINumber]
		, r.[StateNumber]
		, r.[CountyNumber]
		, r.[OperatorName]
		, r.[WellName]
		, r.[Latitude]
		, r.[Longitude]
		, r.[Projection]
		, r.[TVD]
		, r.[TotalBaseWaterVolume]
		, r.[TotalBaseNonWaterVolume]
		, r.[StateName]
		, r.[CountyName]
		, r.[FFVersion]
		, r.[FederalWell]
		, r.[IndianWell]
		, r.[Source]
		, r.[DTMOD]
		, '[RegistryUploadPurpose]' AS [1]
		, p.[pKey]
		, p.[pKeyRegistryUpload] AS [pKeyRegistryUpload(FK)]
		, p.[TradeName]
		, p.[Supplier]
		, p.[Purpose]
		, p.[SystemApproach]
		, p.[IsWater]
		, p.[PercentHFJob]
		, p.[IngredientMSDS]
		, '[RegistryUploadIngredients]' AS [2]
		, i.[pKey]
		, i.[pKeyPurpose] AS [pKeyPurpose(FK)]
		, i.[IngredientName]
		, i.[CASNumber]
		, i.[PercentHighAdditive]
		, i.[PercentHFJob]
		, i.[IngredientComment]
		, i.[IngredientMSDS]
		, i.[MassIngredient]
		, i.[ClaimantCompany]
		, i.[pKeyDisclosure]
FROM	[dbo].[RegistryUpload] AS r
	LEFT JOIN [dbo].[RegistryUploadPurpose] AS p
		ON r.pKey = p.[pKeyRegistryUpload]
	LEFT JOIN [dbo].[RegistryUploadIngredients] AS i
		ON p.[pKey] = i.[pKeyPurpose]
ORDER BY r.[pKey], p.[pKey];
