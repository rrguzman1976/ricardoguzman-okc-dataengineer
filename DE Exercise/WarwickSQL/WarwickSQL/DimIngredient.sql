USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.DimIngredient', N'U') IS NOT NULL
	DROP TABLE dbo.DimIngredient;
GO

-- Need DimIngredient: DimIngredientKey, IngredientName, CASNumber, IngredientComment
CREATE TABLE dbo.DimIngredient
(
	DimIngredientKey		INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, [IngredientName]		VARCHAR(150)		NOT NULL
	, [CASNumber]			VARCHAR(20)			NOT NULL
	, CONSTRAINT IX_DIMINGREDIENT_NAME
		UNIQUE([IngredientName], [CASNumber])
);

INSERT INTO dbo.DimIngredient
SELECT	DISTINCT
		TRIM([IngredientName])
		, TRIM([CASNumber])
FROM	[FracFocusRegistry].[dbo].[RegistryUploadIngredients]
GO

SELECT	*
FROM	dbo.DimIngredient;
