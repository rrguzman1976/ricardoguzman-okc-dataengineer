USE WarwickDB;
GO

IF OBJECT_ID(N'dbo.DimSupplier', N'U') IS NOT NULL
	DROP TABLE dbo.DimSupplier;
GO

-- Need DimSupplier: DimSupplierKey, Supplier
CREATE TABLE dbo.DimSupplier
(
	DimSupplierKey		INT IDENTITY(1, 1)	NOT NULL
		PRIMARY KEY
	, [Supplier]		VARCHAR(150)		NULL
		UNIQUE
);
GO

WITH cleanse
AS
(
	SELECT	CASE
				WHEN [Supplier] IS NULL THEN 'Unknown'
				WHEN TRIM([Supplier]) = '' THEN 'Unknown'
				ELSE TRIM([Supplier])
			END AS [Supplier]
	FROM	[FracFocusRegistry].[dbo].[RegistryUploadPurpose]
)
INSERT INTO dbo.DimSupplier
SELECT	DISTINCT [Supplier]
FROM	cleanse;
GO

SELECT	*
FROM	dbo.DimSupplier
WHERE	Supplier IN ('Alkyl  pyridinium chloride', 'BWA Water Additive US LLC');

